function makeRequest(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                callback(response);
            } else {
                console.error("Request failed with status:", xhr.status);
            }
        }
    };
    xhr.open("GET", url, true);
    xhr.send();
}

function displayFilms(films) {
    const filmsList = document.getElementById("films-list");
    filmsList.style.color = '#c1baba'
    filmsList.style.display = "grid";
    filmsList.style.gridTemplateColumns = "repeat(6, 1fr)";
    filmsList.style.gridTemplateRows = "repeat(1, 1fr)";


    films.forEach(function (film) {
        const filmHtml = `<div class="film-item">
                              <h2 class="film-title">Episode ${film.episodeId}: ${film.name}</h2>
                              <p class='loader'></p>
                          </div>`;
        filmsList.innerHTML += filmHtml;


        Promise.all(film.characters.map(function (characterUrl) {
            return new Promise(function(resolve, reject) {
                makeRequest(characterUrl, function(character) {
                    resolve(character);
                });
            });
        })).then(function (characters) {
            const charactersHtml = characters.map(function (character) {
                return `<p>Character: ${character.name}</p>`;
            }).join("");
            const loadingElement = filmsList.querySelector(".loader");
            if (loadingElement) {
                loadingElement.innerHTML = charactersHtml;
                loadingElement.classList.remove("loader");
            } else {
                console.error("Loading element not found.");
            }
        });
    });
}

makeRequest("https://ajax.test-danit.com/api/swapi/films", function(data) {
    displayFilms(data);
});
